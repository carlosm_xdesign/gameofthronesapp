package com.xdesign.gameofthrones.shared.services

import com.xdesign.gameofthrones.shared.models.Book
import com.xdesign.gameofthrones.shared.models.House
import com.xdesign.gameofthrones.shared.models.Character
import com.xdesign.gameofthrones.shared.results.Result

interface BooksService {
    suspend fun fetchBooks(): Result<Error, List<Book>>

    suspend fun fetchCharacters(): Result<Error, List<Character>>

    suspend fun fetchHouses(): Result<Error, List<House>>
}
