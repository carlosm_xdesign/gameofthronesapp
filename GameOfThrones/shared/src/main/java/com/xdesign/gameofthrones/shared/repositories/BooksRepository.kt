package com.xdesign.gameofthrones.shared.repositories

import com.xdesign.gameofthrones.shared.models.Book
import com.xdesign.gameofthrones.shared.models.Character
import com.xdesign.gameofthrones.shared.models.House
import com.xdesign.gameofthrones.shared.results.Result

interface BooksRepository {
    suspend fun getBooks(): Result<Error, List<Book>>

    suspend fun getCharacters(): Result<Error, List<Character>>

    suspend fun getHouses(): Result<Error, List<House>>
}
