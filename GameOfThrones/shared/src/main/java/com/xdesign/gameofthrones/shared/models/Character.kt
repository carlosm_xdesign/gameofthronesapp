package com.xdesign.gameofthrones.shared.models

data class Character(
    val url: String = "",
    val name: String = "",
    val gender: String = "",
    val culture: String = "",
    val born: String = "",
    val died: String? = "",
    val titles: List<String> = emptyList(),
    val aliases: List<String> = emptyList(),
    val father: String = "",
    val mother: String = "",
    val spouse: String = "",
    val allegiances: List<String> = emptyList(),
    val books: List<String> = emptyList(),
    val povBooks: List<String> = emptyList(),
    val tvSeries: List<String> = emptyList(),
    val playedBy: List<String> = emptyList(),
)
