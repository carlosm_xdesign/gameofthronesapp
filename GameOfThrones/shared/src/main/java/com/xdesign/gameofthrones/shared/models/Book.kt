package com.xdesign.gameofthrones.shared.models

import com.xdesign.gameofthrones.shared.extensions.NEVER
import org.joda.time.Instant


data class Book(
    val url: String = "",
    val name: String = "",
    val isbn: String = "",
    val authors: List<String> = emptyList(),
    val numberOfPages: Int = 0,
    val publisher: String = "",
    val country: String = "",
    val mediaType: String = "",
    val released: Instant = NEVER,
    val characters: List<String> = emptyList(),
    val povCharacters: List<String> = emptyList(),
)
