package com.xdesign.gameofthrones.shared.extensions

import org.joda.time.DateTime
import org.joda.time.Instant

val NEVER = DateTime(0, 1, 1, 0, 0).toInstant()

val Instant.isNotNever get() = this != NEVER
