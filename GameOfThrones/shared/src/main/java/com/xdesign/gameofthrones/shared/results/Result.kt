package com.xdesign.gameofthrones.shared.results

sealed class Result<out ERROR_VALUE, out SUCCESS_VALUE> {
    data class Success<out SUCCESS_VALUE>(val successValue: SUCCESS_VALUE): Result<Nothing, SUCCESS_VALUE>()
    data class Failure<out ERROR_VALUE>(val errorValue: ERROR_VALUE): Result<ERROR_VALUE, Nothing>()

    val isFailure get() = this is Failure<ERROR_VALUE>
    val isSuccess get() = this is Success<SUCCESS_VALUE>
    val value get() = (this as Success).successValue
    val errorBody get() = (this as Failure).errorValue

    fun <T>onResult(onFailure: (ERROR_VALUE) -> T, onSuccess: (SUCCESS_VALUE) -> T) : T =
        when(this) {
            is Success -> onSuccess(successValue)
            is Failure -> onFailure(errorValue)
        }

    fun <T> mapError(onFailure: (ERROR_VALUE) -> T) : Result<T, SUCCESS_VALUE> =
        when (this) {
            is Failure -> Failure(onFailure(this.errorValue))
            is Success -> this
        }

    fun <T> mapTo(onSuccess: (SUCCESS_VALUE) -> T) : Result<ERROR_VALUE, T> =
        when (this) {
            is Failure -> this
            is Success -> Success(onSuccess(successValue))
        }

}
