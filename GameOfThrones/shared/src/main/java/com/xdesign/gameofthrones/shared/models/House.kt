package com.xdesign.gameofthrones.shared.models

data class House(
    val url: String = "",
    val name: String = "",
    val region: String = "",
    val coatOfArms: String = "",
    val words: String = "",
    val currentLordUrl: String = "",
    val heirUrl: String = "",
    val overlordUrl: String = "",
    val founded: String = "",
    val founder: String = "",
    val diedOut: String = "",
    val ancestralWeapons: List<String> = emptyList(),
    val titles: List<String> = emptyList(),
    val seats: List<String> = emptyList(),
    val cadetBranches: List<String> = emptyList(),
    val swornMembersUrls: List<String> = emptyList(),
)
