package com.xdesign.gameofthrones.shared.testing

import io.mockk.MockKAnnotations
import org.junit.Before

open class UnitTest {

    @Before
    open fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }
}
