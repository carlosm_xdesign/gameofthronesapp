package com.xdesign.gameofthrones.services.factories

import com.xdesign.gameofthrones.shared.models.House
import com.xdesign.gameofthrones.services.testing.TestDataFactory
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test


class HousesFactoryShould {

    @Test
    fun `map a house response to a list of houses correctly`() {
        val houses = TestDataFactory.makeApiHouses().mapToHousesModels()
        assertThat(houses.size, equalTo(4))
        assertThatWeGotAllTheHouses(houses)
        assertThatFirstHouseIsCorrect(houses.first())
    }

    private fun assertThatWeGotAllTheHouses(houses: List<House>) {
        listOf("House Algood", "House Allyrion of Godsgrace", "House Amber").forEachIndexed { index, expected ->
            assertThat(houses[index].name, equalTo(expected))
        }
    }

    private fun assertThatFirstHouseIsCorrect(house: House) {
        assertThat("url", house.url, equalTo("https://anapioficeandfire.com/api/houses/1"))
        assertThat("name", house.name, equalTo("House Algood"))
        assertThat("region", house.region, equalTo("The Westerlands"))
        assertThat("words", house.words, equalTo("word"))
        assertThat("coat of arms", house.coatOfArms, equalTo("TLDR"))
        assertThat("titles", house.titles.first(), equalTo("title"))
        assertThat("seat", house.seats.first(), equalTo("seat"))
        assertThat("lord", house.currentLordUrl, equalTo("Laird"))
        assertThat("heir", house.heirUrl, equalTo("heir"))
        assertThat("founder", house.founder, equalTo("Obama"))
        assertThat("overlord", house.overlordUrl, equalTo("https://anapioficeandfire.com/api/houses/229"))
        assertThat("founded", house.founded, equalTo("2000-07-12T00:00:00"))
        assertThat("died out", house.diedOut, equalTo("2011-07-12T00:00:01"))
        assertThat("ancestral weapons", house.ancestralWeapons.first(), equalTo("axe"))
        assertThat("cadet Branches", house.cadetBranches.first(), equalTo("cadet"))
        assertThat("sworn Members", house.swornMembersUrls.first(), equalTo("Bugs Bunny"))
    }
}

