package com.xdesign.gameofthrones.services.factories

import com.xdesign.gameofthrones.shared.models.Book
import com.xdesign.gameofthrones.services.testing.TestDataFactory
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.joda.time.Instant
import org.junit.Test

class BooksFactoryShould {

    @Test
    fun `map a book response to a list of books correctly`() {
        val books = TestDataFactory.makeApiBooks().mapToBookModels()
        assertThat(books.size, equalTo(4))
        assertThatWeGotAllTheBooks(books)
        assertThatSecondBookIsCorrect(books[1])
    }

    private fun assertThatWeGotAllTheBooks(books: List<Book>) {
        listOf("978-0553103540", "978-0553801477", "978-0765332066").forEachIndexed() { index, expected ->
            assertThat(books[index].isbn, equalTo(expected))
        }
    }

    private fun assertThatSecondBookIsCorrect(book: Book) {
        assertThat("url", book.url, equalTo("https://anapioficeandfire.com/api/books/8"))
        assertThat("name", book.name, equalTo("A Dance with Dragons"))
        assertThat("isbn", book.isbn, equalTo("978-0553801477"))
        assertThat("number of authors", book.authors.size, equalTo(1))
        assertThat("first author", book.authors[0], equalTo("George R. R. Martin"))
        assertThat("numb pages", book.numberOfPages, equalTo(1040))
        assertThat("publisher", book.publisher, equalTo("Bantam Books"))
        assertThat("country", book.country, equalTo("United States"))
        assertThat("media type", book.mediaType, equalTo("Hardcover"))
        assertThat("release", book.released, equalTo(Instant("2011-07-12T00:00:00")))
        listOf(
            "https://anapioficeandfire.com/api/characters/2",
            "https://anapioficeandfire.com/api/characters/4",
            "https://anapioficeandfire.com/api/characters/11",
            "https://anapioficeandfire.com/api/characters/12",
            "https://anapioficeandfire.com/api/characters/15",
            "https://anapioficeandfire.com/api/characters/16",
            "https://anapioficeandfire.com/api/characters/19",
            "https://anapioficeandfire.com/api/characters/21",
            "https://anapioficeandfire.com/api/characters/22"
        ).forEachIndexed{ index, character ->
            assertThat("character $index", book.characters[index], equalTo(character))
        }
        listOf(
            "https://anapioficeandfire.com/api/characters/148",
            "https://anapioficeandfire.com/api/characters/150"
        ).forEachIndexed{ index, character ->
            assertThat("character $index", book.povCharacters[index], equalTo(character))
        }
    }
}
