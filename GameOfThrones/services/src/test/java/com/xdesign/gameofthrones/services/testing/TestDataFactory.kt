package com.xdesign.gameofthrones.services.testing

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.xdesign.gameofthrones.services.data.ApiBook
import com.xdesign.gameofthrones.services.data.ApiCharacter
import com.xdesign.gameofthrones.services.data.ApiHouse
import com.xdesign.gameofthrones.services.factories.mapToBookModels
import com.xdesign.gameofthrones.services.factories.mapToCharacterModels
import com.xdesign.gameofthrones.services.factories.mapToHousesModels

object TestDataFactory {
    private const val BOOKS_FILE = "books_response.json"
    private const val CHARACTERS_FILE = "characters_response.json"
    private const val HOUSES_FILE = "houses_response.json"

    private fun loadRawResponse(fileName: String) = FileTools.loadFile(fileName)

    fun makeApiBooks(): List<ApiBook> {
        return Gson().fromJson(loadRawResponse(BOOKS_FILE), object : TypeToken<List<ApiBook>>() {}.type)
    }

    fun makeApiCharacters(): List<ApiCharacter> {
        return Gson().fromJson(loadRawResponse(CHARACTERS_FILE), object : TypeToken<List<ApiCharacter>>() {}.type)
    }

    fun makeApiHouses(): List<ApiHouse> {
        return Gson().fromJson(loadRawResponse(HOUSES_FILE), object : TypeToken<List<ApiHouse>>() {}.type)
    }

    fun makeBooks() = makeApiBooks().mapToBookModels()
    fun makeCharacters() = makeApiCharacters().mapToCharacterModels()
    fun makeHouses() = makeApiHouses().mapToHousesModels()

}
