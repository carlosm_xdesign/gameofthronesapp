package com.xdesign.gameofthrones.services.testing

import java.io.*

object FileTools {

    fun loadFile(filename: String): String {
        val resource = javaClass.classLoader.getResource(filename)
            ?: throw  FileNotFoundException(filename)
        return StringBuffer().apply {
            File(resource.toURI()).forEachLine {
                append(it).append("\n")
            }
        }.toString()
    }
}
