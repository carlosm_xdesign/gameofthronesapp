package com.xdesign.gameofthrones.services.factories

import com.xdesign.gameofthrones.shared.models.Character
import com.xdesign.gameofthrones.services.testing.TestDataFactory
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class CharactersFactoryShould {

    @Test
    fun `map an api characters response to a list of characters correctly`() {
        val characters = TestDataFactory.makeApiCharacters().mapToCharacterModels()
        assertThat(characters.size, equalTo(4))
        assertThatWeGotAllTheCharacters(characters)
        assertThatSecondBookIsCorrect(characters[1])
    }

    private fun assertThatWeGotAllTheCharacters(characters: List<Character>) {
        listOf("Minie", "Walder", "Donald", "Daisy").forEachIndexed { index, expected ->
            assertThat(characters[index].name, equalTo(expected))
        }
    }

    private fun assertThatSecondBookIsCorrect(character: Character) {
        assertThat("url", character.url, equalTo("https://anapioficeandfire.com/api/characters/2"))
        assertThat("name", character.name, equalTo("Walder"))
        assertThat("gender", character.gender, equalTo("Male"))
        assertThat("culture", character.culture, equalTo("Barbarian"))
        assertThat("born", character.born, equalTo("2011-07-12T00:00:00"))
        assertThat("dies", character.died, equalTo("2011-07-12T00:00:01"))
        listOf("Lord of the Flies", "Crow's Ride").forEachIndexed { index, expected ->
            assertThat("Title: $expected", character.titles[index], equalTo(expected))
        }
        assertThat("aliases", character.aliases.first(), equalTo("Hodor"))
        assertThat("father", character.father, equalTo("Old Hodor"))
        assertThat("mother", character.mother, equalTo("Hodorina"))
        assertThat("spouse", character.spouse, equalTo("Hodorette"))
        listOf(
            "https://anapioficeandfire.com/api/books/1",
            "https://anapioficeandfire.com/api/books/2"
        ).forEachIndexed { index, expected ->
            assertThat("Book: $expected", character.books[index], equalTo(expected))
        }
        listOf(
            "https://anapioficeandfire.com/api/books/3",
            "https://anapioficeandfire.com/api/books/4",
        ).forEachIndexed { index, expected ->
            assertThat("povBook: $expected", character.povBooks[index], equalTo(expected))
        }
        listOf("Season 1", "Season 2", "Season 3", "Season 4", "Season 6").forEachIndexed { index, expected ->
            assertThat("Series: $expected", character.tvSeries[index], equalTo(expected))
        }
        assertThat("played by", character.playedBy.first(), equalTo("Kristian Nairn"))
    }
}
