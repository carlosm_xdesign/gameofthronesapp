package com.xdesign.gameofthrones.services.factories

import com.xdesign.gameofthrones.services.data.ApiCharacter
import com.xdesign.gameofthrones.shared.models.Character

@Suppress("ComplexMethod")
fun List<ApiCharacter?>?.mapToCharacterModels(): List<Character> {
    return this?.filterNotNull()?.map {
        Character(
            url = it.url ?: "",
            name = it.name ?: "",
            gender = it.gender ?: "",
            culture = it.culture ?: "",
            born = it.born ?: "",
            died = it.died ?: "",
            father = it.father ?: "",
            mother = it.mother ?: "",
            spouse = it.spouse ?: "",
            titles = it.titles?.filterNotNull() ?: emptyList(),
            aliases = it.aliases?.filterNotNull() ?: emptyList(),
            allegiances = it.allegiances?.filterNotNull() ?: emptyList(),
            books = it.books?.filterNotNull() ?: emptyList(),
            povBooks = it.povBooks?.filterNotNull() ?: emptyList(),
            tvSeries = it.tvSeries?.filterNotNull() ?: emptyList(),
            playedBy = it.playedBy?.filterNotNull() ?: emptyList(),
        )
    } ?: emptyList()
}
