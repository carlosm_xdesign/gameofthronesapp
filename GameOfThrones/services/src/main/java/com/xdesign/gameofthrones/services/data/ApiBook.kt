package com.xdesign.gameofthrones.services.data

data class ApiBook(
    val url: String? = null,
    val name: String? = null,
    val isbn: String? = null,
    val authors: List<String?>? = null,
    val numberOfPages: Int? = null,
    val publisher: String? = null,
    val country: String? = null,
    val mediaType: String? = null,
    val released: String? = null,
    val characters: List<String?>? = null,
    val povCharacters: List<String?>? = null
)

