package com.xdesign.gameofthrones.services

import com.xdesign.gameofthrones.services.factories.mapToBookModels
import com.xdesign.gameofthrones.services.factories.mapToCharacterModels
import com.xdesign.gameofthrones.services.factories.mapToHousesModels
import com.xdesign.gameofthrones.shared.models.Book
import com.xdesign.gameofthrones.shared.models.Character
import com.xdesign.gameofthrones.shared.models.House
import com.xdesign.gameofthrones.shared.results.Result
import com.xdesign.gameofthrones.shared.results.Result.Failure
import com.xdesign.gameofthrones.shared.results.Result.Success
import com.xdesign.gameofthrones.shared.services.BooksService
import retrofit2.Response
import retrofit2.Retrofit

class BooksServiceProvider(private val retrofit: Retrofit) : BooksService {

    private val booksService by lazy { retrofit.create(BooksApi::class.java) }

    override suspend fun fetchBooks(): Result<Error, List<Book>> =
        booksService.fetchBooks().let {
            return if (it.isSuccessful) Success(it.body().mapToBookModels())
            else makeFailureFrom(it)
        }

    override suspend fun fetchCharacters(): Result<Error, List<Character>> =
        booksService.fetchCharacters().let {
            return if (it.isSuccessful) Success(it.body().mapToCharacterModels())
            else makeFailureFrom(it)
        }

    override suspend fun fetchHouses(): Result<Error, List<House>> =
        booksService.fetchHouses().let {
            return if (it.isSuccessful) Success(it.body().mapToHousesModels())
            else makeFailureFrom(it)
        }

    private fun <T> makeFailureFrom(response: Response<T>) =
        Failure(Error("Error (${response.code()}): ${response.message()}"))
}
