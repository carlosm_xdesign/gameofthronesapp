package com.xdesign.gameofthrones.services
import com.xdesign.gameofthrones.services.data.ApiBook
import com.xdesign.gameofthrones.services.data.ApiCharacter
import com.xdesign.gameofthrones.services.data.ApiHouse
import retrofit2.Response
import retrofit2.http.GET

interface BooksApi {
    @GET("api/books")
    suspend fun fetchBooks(): Response<List<ApiBook>>

    @GET("api/characters")
    suspend fun fetchCharacters(): Response<List<ApiCharacter>>

    @GET("api/houses")
    suspend fun fetchHouses(): Response<List<ApiHouse>>
}

