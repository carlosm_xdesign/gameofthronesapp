package com.xdesign.gameofthrones.services.factories

import com.xdesign.gameofthrones.services.data.ApiBook
import com.xdesign.gameofthrones.shared.models.Book
import org.joda.time.Instant

@Suppress("ComplexMethod")
fun List<ApiBook?>?.mapToBookModels(): List<Book> {
    return this?.filterNotNull()?.map {
        Book(
            url = it.url ?: "",
            name = it.name ?: "",
            isbn = it.isbn ?: "",
            authors = it.authors?.filterNotNull() ?: emptyList(),
            numberOfPages = it.numberOfPages ?: 0,
            publisher = it.publisher ?: "",
            country = it.country ?: "",
            mediaType = it.mediaType ?: "",
            released = Instant(it.released ?: ""),
            characters = it.characters?.filterNotNull() ?: emptyList(),
            povCharacters = it.povCharacters?.filterNotNull() ?: emptyList()
        )
    } ?: emptyList()
}
