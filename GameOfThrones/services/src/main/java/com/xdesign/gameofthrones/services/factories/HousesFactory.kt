package com.xdesign.gameofthrones.services.factories

import com.xdesign.gameofthrones.services.data.ApiHouse
import com.xdesign.gameofthrones.shared.models.House

@Suppress("ComplexMethod")
fun List<ApiHouse?>?.mapToHousesModels(): List<House> {
    return this?.filterNotNull()?.map {
        House(
            url = it.url ?: "",
            name = it.name ?: "",
            region = it.region ?: "",
            coatOfArms = it.coatOfArms ?: "",
            words = it.words ?: "",
            currentLordUrl = it.currentLord ?: "",
            heirUrl = it.heir ?: "",
            overlordUrl = it.overlord ?: "",
            founded = it.founded ?: "",
            diedOut = it.diedOut ?: "",
            founder = it.founder ?: "",
            titles = it.titles?.filterNotNull() ?: emptyList(),
            seats = it.seats?.filterNotNull() ?: emptyList(),
            ancestralWeapons = it.ancestralWeapons?.filterNotNull() ?: emptyList(),
            cadetBranches = it.cadetBranches?.filterNotNull() ?: emptyList(),
            swornMembersUrls = it.swornMembers?.filterNotNull() ?: emptyList(),
        )
    } ?: emptyList()
}
