package com.xdesign.gameofthrones.services

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.xdesign.gameofthrones.shared.network.Environment
import com.xdesign.gameofthrones.shared.services.BooksService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

interface ServicesModuleFactory {
    fun build(environment: Environment): Module
}

class ServicesModuleFabricator : ServicesModuleFactory {

    companion object {
        const val TIMEOUT_SECONDS = 30L
    }

    override fun build(environment: Environment): Module {
        return module {

            fun provideGson(): Gson {
                return GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create()
            }

            fun provideHttpClient(): OkHttpClient {
                val interceptor = HttpLoggingInterceptor()
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                val okHttpClientBuilder = OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
                return okHttpClientBuilder.build()
            }

            fun provideRetrofit(factory: Gson, client: OkHttpClient): Retrofit {
                return Retrofit.Builder()
                    .baseUrl(environment.apiUrl)
                    .addConverterFactory(GsonConverterFactory.create(factory))
                    .client(client)
                    .build()
            }

            fun provideBookService(retrofit: Retrofit): BooksService {
                return BooksServiceProvider(retrofit)
            }

            factory { provideBookService(get()) }
            factory { provideGson() }
            factory { provideHttpClient() }
            factory { provideRetrofit(get(), get()) }
        }
    }
}
