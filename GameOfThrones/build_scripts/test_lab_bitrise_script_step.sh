#!/bin/bash
# fail if any commands fails
set -e
# debug log
set -x

function get_build_scripts_path {
  local folder="build_scripts"
  if [ -z "$PROJECT_LOCATION" ] # is Empty?
  then
       echo "./$folder"
  else
       echo "$PROJECT_LOCATION/$folder"
  fi
}

SCRIPTS_PATH=$(get_build_scripts_path)
echo "scripts path: $SCRIPTS_PATH"

<<'###BLOCK-COMMENT'

  Import Script Dependencies

  note:
    ShellCheck is not able to include sourced files from paths that are
    determined at runtime. The file will not be read, potentially resulting
    in warnings about unassigned variables and similar.
    Use a Directive to point shellcheck to a fixed location it can read instead.
###BLOCK-COMMENT
#
# shellcheck source=./activate_gcloud_service_account.sh
source "$SCRIPTS_PATH/activate_gcloud_service_account.sh"
# shellcheck source=./check_file_exists.sh
source "$SCRIPTS_PATH/check_file_exists.sh"
# shellcheck source=./download_gcloud_key.sh
source "$SCRIPTS_PATH/download_gcloud_key.sh"
# shellcheck source=./print_step.sh
source "$SCRIPTS_PATH/print_step.sh"
# shellcheck source=./run_test_lab_task.sh
source "$SCRIPTS_PATH/run_test_lab_task.sh"

print_step "Starting, project location: $PROJECT_LOCATION"

###################################################################
#
# This Script's Variables
#
###################################################################
#

PATH_TO_APK="$PROJECT_LOCATION/app/build/outputs/apk/flavorInstrumentation/debug/app-flavorInstrumentation-debug.apk"
PATH_TO_TEST_APK="$PROJECT_LOCATION/app/build/outputs/apk/androidTest/flavorInstrumentation/debug"
KEY_FILE="gcloud_key.json"
GCLOUD_KEY_URL=$BITRISEIO_GCLOUD_JSON_URL

###################################################################
#
# Run Tasks
#
###################################################################
#
function execute {
  download_gcloud_key "$GCLOUD_KEY_URL" $KEY_FILE
  check_file_exists $KEY_FILE
  activate_gcloud_service_account $KEY_FILE
  check_file_exists "$PATH_TO_APK"
  run_test_lab_task "$PATH_TO_APK"
}

execute
exit 0
