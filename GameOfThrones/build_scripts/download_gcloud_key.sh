echo "sourced download_gcloud_key"

function download_gcloud_key {
  print_step "Downloading gcloud key from: $1 to $2"
  curl $1 --output $2
}