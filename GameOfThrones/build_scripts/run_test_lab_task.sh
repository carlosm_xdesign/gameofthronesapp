echo "sourced run_test_lab_task"

function run_test_lab_task {
  print_step "Running Firebase Test Lab for apk: $1"
  gcloud firebase test android run \
    --project carlos-got-app \
    --type robo \
    --app "$1" \
    --device model=Pixel2,version=30,locale=en,orientation=portrait \
    --timeout 90s
}