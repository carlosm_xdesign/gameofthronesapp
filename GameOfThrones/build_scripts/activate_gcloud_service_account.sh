echo "sourced activate_gcloud_service_account"

function activate_gcloud_service_account {
  print_step "Activating gcloud Service Account: $1"
  gcloud auth activate-service-account -q --key-file "$1"
  gcloud auth list
}