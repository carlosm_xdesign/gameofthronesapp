echo "sourced check_file_exists"

function check_file_exists {
  printf "Checking that file exists: \n"
  if [ -f "$1" ]; then
    echo "$1 exists."
  else
    raise error "$1 does not exist."
  fi
}