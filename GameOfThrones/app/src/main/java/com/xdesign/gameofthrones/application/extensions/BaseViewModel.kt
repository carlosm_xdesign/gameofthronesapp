package com.xdesign.gameofthrones.application.extensions

import androidx.lifecycle.ViewModel
import com.xdesign.gameofthrones.application.coroutines.AppScopeLauncher
import com.xdesign.gameofthrones.application.coroutines.ScopeLauncher

abstract class BaseViewModel(protected val scopeLauncher: ScopeLauncher = AppScopeLauncher()) :
    ViewModel() {

    override fun onCleared() {
        super.onCleared()
        scopeLauncher.cancel()
    }
}

