package com.xdesign.gameofthrones.application.modules

import android.app.Application
import com.xdesign.gameofthrones.services.ServicesModuleFabricator
import com.xdesign.gameofthrones.shared.network.Environment
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

object KoinBooter {
    fun start(app: Application, environment: Environment) {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(app)
            buildAppModules(environment)
        }
    }

    fun KoinApplication.buildAppModules(environment: Environment) = modules(
        mutableListOf(
            appModules,
            storeModules,
            useCaseModules,
            viewModules,
            ServicesModuleFabricator().build(environment)
        )
    )
}
