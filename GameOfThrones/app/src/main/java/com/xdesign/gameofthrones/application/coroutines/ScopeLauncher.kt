package com.xdesign.gameofthrones.application.coroutines

import kotlinx.coroutines.CoroutineScope

interface ScopeLauncher {
    val scope: CoroutineScope
    fun launch(task: suspend () -> Unit)
    fun cancel()
}