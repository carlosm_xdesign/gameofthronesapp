package com.xdesign.gameofthrones.application.modules

import com.xdesign.gameofthrones.home.HomeViewModel
import com.xdesign.gameofthrones.widgets.booklist.BookListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModules = module {
    viewModel { HomeViewModel() }
    viewModel { BookListViewModel(get(), get()) }
}
