package com.xdesign.gameofthrones.application.modules

import com.xdesign.gameofthrones.core.books.BooksUseCase
import com.xdesign.gameofthrones.core.books.UsingBooks
import org.koin.dsl.module

val useCaseModules = module {
    factory<BooksUseCase> { UsingBooks(get()) }
}
