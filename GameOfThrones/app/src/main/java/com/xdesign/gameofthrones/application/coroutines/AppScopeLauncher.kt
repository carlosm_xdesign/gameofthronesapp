package com.xdesign.gameofthrones.application.coroutines

import kotlinx.coroutines.*

class AppScopeLauncher : ScopeLauncher {
    override val scope = CoroutineScope(Dispatchers.IO)
    private var currentJob: Job? = null

    override fun launch(task: suspend () -> Unit) {
        currentJob = scope.launch { task() }
    }

    override fun cancel() {
        scope.cancel()
    }


}