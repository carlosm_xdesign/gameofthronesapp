package com.xdesign.gameofthrones.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.xdesign.gameofthrones.databinding.ViewMyViewBinding


class MyView : ConstraintLayout {

    private var binding: ViewMyViewBinding? = null

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        binding = ViewMyViewBinding.inflate(LayoutInflater.from(context), this, true)
        binding?.labelText = "Pickles"
    }
}