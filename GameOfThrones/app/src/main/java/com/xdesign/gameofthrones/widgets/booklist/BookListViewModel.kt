package com.xdesign.gameofthrones.widgets.booklist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.xdesign.gameofthrones.application.coroutines.ScopeLauncher
import com.xdesign.gameofthrones.application.extensions.BaseViewModel
import com.xdesign.gameofthrones.core.books.BooksUseCase
import com.xdesign.gameofthrones.shared.models.Book
import org.joda.time.format.DateTimeFormat

class BookListViewModel(private val usingBooks: BooksUseCase, scopeLauncher: ScopeLauncher) :
    BaseViewModel(scopeLauncher) {

    companion object {
        const val NO_BOOKS_FOUND_MSG = "No Books Found"
    }

    val books = MutableLiveData<List<BookListDisplayBook>>()
    val isLoading = MutableLiveData<Boolean?>(null)
    val screenMessage = MutableLiveData("")
    val showScreenMessage = Transformations.map(screenMessage) { it.isNotEmpty() }

    fun onViewReady() {
        showLoadingIndicator()
        scopeLauncher.launch {
            usingBooks.getBooksOrderedByRecency().onResult(
                this@BookListViewModel::showErrorFetchingBooks,
                this@BookListViewModel::displayFetchedBooks
            )
        }
    }

    private fun displayFetchedBooks(bookList: List<Book>) {
        stopLoadingIndicator()
        books.postValue(bookList.toDisplayBooks)
        if (bookList.isEmpty()) showMessage(NO_BOOKS_FOUND_MSG)
        else clearMessage()
    }

    private fun showErrorFetchingBooks(error: Error) {
        stopLoadingIndicator()
        showMessage(error.message.toString())
    }

    private fun showMessage(message: String) = screenMessage.postValue(message)
    private fun clearMessage() = screenMessage.postValue("")

    private fun stopLoadingIndicator() = isLoading.postValue(false)
    private fun showLoadingIndicator() = isLoading.postValue(true)

    val List<Book>.toDisplayBooks: List<BookListDisplayBook> get() = map {
        BookListDisplayBook(
            isbn = it.isbn,
            name = it.name,
            releasedOn = DateTimeFormat.forPattern("MM/dd/yyyy").print(it.released))
    }
}
