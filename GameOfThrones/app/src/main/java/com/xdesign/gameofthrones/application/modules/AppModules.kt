package com.xdesign.gameofthrones.application.modules

import com.xdesign.gameofthrones.application.coroutines.AppScopeLauncher
import com.xdesign.gameofthrones.application.coroutines.ScopeLauncher
import org.koin.dsl.module

val appModules = module {
    factory<ScopeLauncher> { AppScopeLauncher() }
}
