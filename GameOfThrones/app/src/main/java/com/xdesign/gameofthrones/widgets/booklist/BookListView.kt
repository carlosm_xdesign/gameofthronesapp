package com.xdesign.gameofthrones.widgets.booklist

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import com.airbnb.epoxy.EpoxyRecyclerView
import com.xdesign.gameofthrones.R
import com.xdesign.gameofthrones.bookListItem
import com.xdesign.gameofthrones.databinding.ViewBookListBinding
import com.xdesign.gameofthrones.widgets.extentions.ScopedView
import org.koin.java.KoinJavaComponent.inject

class BookListView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttr: Int = 0
) : ScopedView<BookListViewModel>(context, attrs, defStyleAttr) {

    private val booksListViewViewModel: BookListViewModel by inject(BookListViewModel::class.java)
    private lateinit var binding: ViewBookListBinding


    override fun initView(context: Context, attrs: AttributeSet?) {
         binding = ViewBookListBinding.inflate(LayoutInflater.from(context), this, true)
    }

    override fun createViewModel(fragment: Fragment): BookListViewModel = booksListViewViewModel

    override fun onCreatedViewModel(viewModel: BookListViewModel) {
        scopeFragment?.viewLifecycleOwner?.let { viewLifecycleOwner ->
            binding.model = model
            binding.lifecycleOwner = viewLifecycleOwner
            model?.books?.observe(viewLifecycleOwner) { books ->
                findViewById<EpoxyRecyclerView>(R.id.book_list)?.apply {
                    withModels {
                        books.forEach {
                            bookListItem {
                                id(it.isbn)
                                book(it)
                            }
                        }
                    }
                }
            }
        }
    }

    fun setup(scope: Fragment) {
        super.scopeTo(scope)
        model?.onViewReady()
    }
}


