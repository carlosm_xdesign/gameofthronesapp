package com.xdesign.gameofthrones.application.modules

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.xdesign.gameofthrones.application.modules.KoinBooter.buildAppModules
import com.xdesign.gameofthrones.shared.network.Environment
import com.xdesign.gameofthrones.shared.services.BooksService
import io.mockk.mockkClass
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.logger.Level
import org.koin.test.KoinTestRule
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock

@RunWith(JUnit4::class)
class KoinBooterShould : KoinBooterTestInjects() {

    @get:Rule
    var instantExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger(Level.ERROR)
        buildAppModules(Environment())
    }

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz -> mockkClass(clazz) }

    @Before
    fun setup() {
        declareMock<BooksService>()
    }

    @Test
    fun `provide and instance of every dependency`()
        = dependencies.forEach { assertThat(it, notNullValue()) }

    @Test
    fun `provide and instance of every view model dependency`()
        = viewModelDependencies.forEach { assertThat(it, notNullValue()) }
}

