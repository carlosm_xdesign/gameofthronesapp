package com.xdesign.gameofthrones.testing

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.xdesign.gameofthrones.application.coroutines.AppScopeLauncher
import com.xdesign.gameofthrones.application.coroutines.ScopeLauncher
import io.mockk.MockKAnnotations
import io.mockk.mockkClass
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.koin.test.KoinTest
import org.koin.test.mock.MockProviderRule

open class BaseViewModelTest : KoinTest {

    lateinit var scopeLauncher: ScopeLauncher

    @get:Rule
    var instantExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz -> mockkClass(clazz) }

    @Before
    open fun injectMocks() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        scopeLauncher = AppScopeLauncher()
    }

    fun awaitCoroutineCompletion() {
        runBlocking {
            scopeLauncher.scope.coroutineContext[Job]!!.children.forEach {
                it.join()
                it.children.forEach { children -> children.join() }
            }
        }
    }
}
