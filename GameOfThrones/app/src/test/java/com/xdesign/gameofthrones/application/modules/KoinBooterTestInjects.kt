package com.xdesign.gameofthrones.application.modules

import com.xdesign.gameofthrones.home.HomeViewModel
import com.xdesign.gameofthrones.shared.repositories.BooksRepository
import com.xdesign.gameofthrones.widgets.booklist.BookListViewModel
import org.koin.test.KoinTest
import org.koin.test.inject

abstract class KoinBooterTestInjects : KoinTest {
    val booksRepository: BooksRepository by inject()
    val homeViewModel: HomeViewModel by inject()
    val bookListViewModel: BookListViewModel by inject ()
//    val nonExistingDependency: Book by inject() // for proofing that a not met dependencies will blow up the tests

    val dependencies: List<Any> by lazy {
        listOf(
//            nonExistingDependency,
            booksRepository,
        )
    }

    val viewModelDependencies: List<Any> by lazy {
        listOf(
            homeViewModel,
            bookListViewModel
        )
    }
}
