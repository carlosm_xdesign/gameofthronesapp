package com.xdesign.gameofthrones.widgets.booklist.testing

import android.app.Activity
import androidx.test.rule.ActivityTestRule
import com.xdesign.gameofthrones.application.coroutines.AppScopeLauncher
import com.xdesign.gameofthrones.application.coroutines.ScopeLauncher
import io.mockk.MockKAnnotations
import io.mockk.mockkClass
import junit.framework.TestCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.koin.test.mock.MockProviderRule

abstract class BaseActivityTest(
    private val clazz: Class<out Activity>,
    private val launchActivity: Boolean = true
) : TestCase() {

    lateinit var scopeLauncher: ScopeLauncher


    /**
     *
     *  source: https://github.com/android/android-test/issues/297
     *
     * DataBinding doesn't use main thread for queueing bindings,
     * so when writing UI tests with Espresso there's need for an IdlingResource,
     * like in the GithubSample repo. However, this sample uses the old ActivityTestRule API.
     *
     * ""With the new ActivityScenario API we no longer have access to the Activity instance
     * directly, instead we use onActivity. The problem is, the method asserts that it's not
     *
     * called on the main thread:
     *      public ActivityScenario<A> onActivity(final ActivityAction<A> action) {
     *          checkNotMainThread();
     *
     *  while Espresso calls IdlingResource#isIdleNow from the main thread only.
     *  Thus the integration is not straightforward.""
     */
    @Rule
    @JvmField
    val activityRule = ActivityTestRule(clazz, true, launchActivity)

    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz -> mockkClass(clazz) }

    @Before
    open fun injectMocks() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        scopeLauncher = AppScopeLauncher()
    }

    @After
    fun after() {
    }

    fun takeScreenshot() {
    }

    fun awaitCoroutineCompletion() {
        runBlocking {
            scopeLauncher.scope.coroutineContext[Job]!!.children.forEach {
                it.join()
                it.children.forEach { children -> children.join() }
            }
        }
    }
}
