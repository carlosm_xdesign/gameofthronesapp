package com.xdesign.gameofthrones.widgets.booklist

import android.view.View
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.agoda.kakao.progress.KProgressBar
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KTextView
import com.xdesign.gameofthrones.BookListTestActivity
import com.xdesign.gameofthrones.R
import com.xdesign.gameofthrones.shared.models.Book
import com.xdesign.gameofthrones.shared.repositories.BooksRepository
import com.xdesign.gameofthrones.shared.results.Result
import com.xdesign.gameofthrones.widgets.booklist.testing.BaseActivityTest
import io.mockk.CoFunctionAnswer
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matcher
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

@RunWith(AndroidJUnit4::class)
@LargeTest
class BookListViewShould : BaseActivityTest(
    BookListTestActivity::class.java, launchActivity = false
) {

    lateinit var screen: BooksListViewScreen

    @RelaxedMockK
    lateinit var booksRepository: BooksRepository

    @Before
    fun setup() {
        val moduleOverride = module(override = true) {
            single { booksRepository }
        }
        loadKoinModules(moduleOverride)
    }

    private fun launchActivity() {
        activityRule.launchActivity(null)
        screen = BooksListViewScreen()
    }

    @Test
    fun not_show_any_message_on_screen_load() = runBlocking {
        coEvery { booksRepository.getBooks() } answers (takeForeverToAnswer)
        launchActivity()
        screen {
            screenMessage.isNotDisplayed()
        }
    }

    @Test
    fun show_a_progress_bar_on_screen_load() = runBlocking {
        coEvery { booksRepository.getBooks() } answers (takeForeverToAnswer)
        launchActivity()
        screen {
            progressBar.isDisplayed()
        }
    }

    @Test
    fun after_trying_to_get_books_do_not_display_loading_spinner() = runBlocking {
        coEvery { booksRepository.getBooks() } answers { Result.Success(emptyList()) }
        launchActivity()
        screen {
            progressBar.isNotDisplayed()
            screenMessage.hasText("No Books Found")
        }
    }

    @Test
    fun display_the_books() = runBlocking {
        Thread.sleep(600)
        coEvery { booksRepository.getBooks() } answers {
            Result.Success(
                listOf(
                    Book(isbn = "1", name = "11 El Book"),
                    Book(isbn = "2", name = "2 El Book"),
                    Book(isbn = "3", name = "3 El Book")
                )
            )
        }
        launchActivity()
        awaitCoroutineCompletion()
        screen {
            booksList {
                firstChild<BookItem> {
                    name { hasText("1 El Book") }
                }
            }
        }
        takeScreenshot()
    }


    inner class BooksListViewScreen : Screen<BooksListViewScreen>() {
        val screenMessage = KTextView {
            withId(R.id.screen_message)
        }
        val progressBar = KProgressBar {
            withId(R.id.progress_bar)
        }
        val booksList: KRecyclerView = KRecyclerView({
            withId(R.id.book_list)
        }, itemTypeBuilder = {
            itemType(::BookItem)
        })
    }

    companion object {
        val takeForeverToAnswer = CoFunctionAnswer<Result<Error, List<Book>>> {
            delay(99999999)
            Result.Success(emptyList())
        }
    }
}

class BookItem(parent: Matcher<View>) : KRecyclerItem<BookItem>(parent) {
    val name: KTextView = KTextView(parent) { withId(R.id.book_name) }
    val date: KTextView = KTextView(parent) { withId(R.id.book_release_date) }
}


