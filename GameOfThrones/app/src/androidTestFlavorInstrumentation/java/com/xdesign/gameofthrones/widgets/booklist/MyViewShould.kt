package com.xdesign.gameofthrones.widgets.booklist

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KTextView
import com.xdesign.gameofthrones.MyViewTestActivity
import com.xdesign.gameofthrones.R
import com.xdesign.gameofthrones.widgets.booklist.testing.BaseActivityTest
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MyViewShould : BaseActivityTest(MyViewTestActivity::class.java) {

    private var screen = MyViewScreenMapper()

    @Test
    fun be_a_pickle() = runBlocking {
        screen {
            labelView.hasText("Pickles")
        }
    }

    class MyViewScreenMapper : Screen<MyViewScreenMapper>() {
        val labelView = KTextView {
            withId(R.id.my_label_text_view)
        }
    }
}



