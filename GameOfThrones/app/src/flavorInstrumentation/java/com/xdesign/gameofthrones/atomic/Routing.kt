package com.xdesign.gameofthrones.atomic

import com.xdesign.gameofthrones.BookListTestActivity
import com.xdesign.gameofthrones.MyViewTestActivity

object Routing {
    val destinations = listOf(
        Destination("BookList", BookListTestActivity::class),
        Destination("MyView", MyViewTestActivity::class)
    )
}