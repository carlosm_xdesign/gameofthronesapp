package com.xdesign.gameofthrones.atomic

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import kotlin.reflect.KClass

class Destination(val name: String, private val destClass: KClass<out AppCompatActivity>) {
    fun go(activity: Activity) {
        activity.startActivity(Intent(activity, destClass.java))
    }
}