package com.xdesign.gameofthrones.atomic

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.xdesign.gameofthrones.databinding.ViewHolderUiComponentItemBinding

class AtomsRecyclerViewAdapter(
    private val values: List<Destination>
) : RecyclerView.Adapter<AtomsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ViewHolderUiComponentItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.itemBinding.buttonLabel = item.name
        holder.itemBinding.atomButton.setOnClickListener {
            item.go(it.context as AppCompatActivity)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: ViewHolderUiComponentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val itemBinding: ViewHolderUiComponentItemBinding = binding

    }

}