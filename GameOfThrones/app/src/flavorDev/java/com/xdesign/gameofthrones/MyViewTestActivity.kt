package com.xdesign.gameofthrones

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.xdesign.gameofthrones.databinding.ActivityMyViewTestBinding

class MyViewTestActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMyViewTestBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBinding()
    }

    private fun setupBinding() {
        binding = ActivityMyViewTestBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}