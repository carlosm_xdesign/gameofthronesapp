package com.xdesign.gameofthrones.core.books

import com.xdesign.gameofthrones.core.testing.UnitTest
import com.xdesign.gameofthrones.shared.models.Book
import com.xdesign.gameofthrones.shared.repositories.BooksRepository
import com.xdesign.gameofthrones.shared.results.Result.Success
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.joda.time.Instant
import org.junit.Test

class UsingBooksShould : UnitTest() {

    @RelaxedMockK
    lateinit var booksRepository: BooksRepository

    @Test
     fun `give us the books sorted by recency`() {
        val responseBooks = listOf(
            Book(name = "One", released = Instant("1996-08-01T00:00:00")),
            Book(name = "Three", released = Instant("1998-08-01T00:00:00")),
            Book(name = "Two", released = Instant("1997-08-01T00:00:00")),
        )
        coEvery { booksRepository.getBooks() } answers { Success(responseBooks) }
        var resultBooks: List<Book>?
        runBlocking {
            val usingBooks = UsingBooks(booksRepository)
            resultBooks = usingBooks.getBooksOrderedByRecency().value
        }
        assertThat(resultBooks!![0].name, equalTo("Three"))
        assertThat(resultBooks!![1].name, equalTo("Two"))
        assertThat(resultBooks!![2].name, equalTo("One"))
    }
}
