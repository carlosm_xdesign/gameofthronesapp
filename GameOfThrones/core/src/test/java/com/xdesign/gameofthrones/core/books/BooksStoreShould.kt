package com.xdesign.gameofthrones.core.books


class BooksStoreShould {

//    @RelaxedMockK
//    lateinit var retrofit: Retrofit
//    @RelaxedMockK
//    lateinit var booksApi: BooksApi
//    lateinit var bookStore: BooksStore
//
//    @Before
//    override fun setup() {
//        super.setup()
//        every { retrofit.create(BooksApi::class.java) }.returns(booksApi)
//        bookStore = BooksStore(retrofit)
//    }
//
//    @Test
//    fun `give us a the list of books as sent by the api`() = runBlocking {
//        coEvery { booksApi.fetchBooks() } returns Response.success(TestDataFactory.makeApiBooks())
//        var books: List<Book>? = null
//        bookStore.getBooks().either(onFailure = {}, onSuccess = { books = it })
//        listOf("978-0553103540", "978-0553801477", "978-0765332066", "978-0345537263").forEachIndexed { index, expected ->
//            assertThat(books!![index].isbn, equalTo(expected))
//        }
//    }
//
//    @Test
//    fun `give us an error when the fetching books fails`()  = runBlocking {
//        coEvery { booksApi.fetchBooks() } returns Response.error(404, mockk(relaxed = true))
//        var error: Error? = null
//        bookStore.getBooks().either(onFailure = { error = it }, onSuccess = {})
//       assertThat(error!!.message, equalTo("Error (404): Response.error()"))
//    }
//
//    @Test
//    fun `give us a the list of houses as sent by the api`() = runBlocking {
//        coEvery { booksApi.fetchHouses() } returns Response.success(TestDataFactory.makeApiHouses())
//        var houses: List<House>? = null
//        bookStore.getHouses().either(onFailure = {}, onSuccess = { houses = it })
//        listOf("House Algood", "House Allyrion of Godsgrace", "House Amber", "House Ambrose").forEachIndexed { index, expected ->
//            assertThat(houses!![index].name, equalTo(expected))
//        }
//    }
//
//    @Test
//    fun `give us an error when the fetching houses fails`()  = runBlocking {
//        coEvery { booksApi.fetchHouses() } returns Response.error(401, mockk(relaxed = true))
//        var error: Error? = null
//        bookStore.getHouses().either(onFailure = { error = it }, onSuccess = {})
//        assertThat(error!!.message, equalTo("Error (401): Response.error()"))
//    }
//
//    @Test
//    fun `give us a the list of characters as sent by the api`() = runBlocking {
//        coEvery { booksApi.fetchCharacters() } returns Response.success(TestDataFactory.makeApiCharacters())
//        var characters: List<Character>? = null
//        bookStore.getCharacters().either(onFailure = {}, onSuccess = { characters = it })
//        listOf("Minie", "Walder", "Donald", "Daisy").forEachIndexed { index, expected ->
//            assertThat(characters!![index].name, equalTo(expected))
//        }
//    }
//
//    @Test
//    fun `give us an error when the fetching character fails`()  = runBlocking {
//        coEvery { booksApi.fetchCharacters() } returns Response.error(401, mockk(relaxed = true))
//        var error: Error? = null
//        bookStore.getCharacters().either(onFailure = { error = it }, onSuccess = {})
//        assertThat(error!!.message, equalTo("Error (401): Response.error()"))
//    }
}
