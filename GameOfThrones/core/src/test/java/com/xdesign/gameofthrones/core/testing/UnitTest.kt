package com.xdesign.gameofthrones.core.testing

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.MockKAnnotations
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.koin.test.KoinTest

open class UnitTest : KoinTest {
    @get:Rule
    var instantExecutorRule: TestRule = InstantTaskExecutorRule()

    @Before
    open fun injectMocks() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        postInjectMocks()
    }

    open fun postInjectMocks() {}

}
