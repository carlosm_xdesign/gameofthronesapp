package com.xdesign.gameofthrones.core.books

import com.xdesign.gameofthrones.shared.models.Book
import com.xdesign.gameofthrones.shared.models.Character
import com.xdesign.gameofthrones.shared.models.House
import com.xdesign.gameofthrones.shared.repositories.BooksRepository
import com.xdesign.gameofthrones.shared.results.Result
import com.xdesign.gameofthrones.shared.services.BooksService

class BooksStore(private val booksService: BooksService) : BooksRepository {

    override suspend fun getBooks(): Result<Error, List<Book>> = booksService.fetchBooks()

    override suspend fun getCharacters(): Result<Error, List<Character>> = booksService.fetchCharacters()

    override suspend fun getHouses(): Result<Error, List<House>> = booksService.fetchHouses()
}
