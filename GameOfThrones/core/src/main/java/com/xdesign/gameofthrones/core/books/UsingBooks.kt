package com.xdesign.gameofthrones.core.books

import com.xdesign.gameofthrones.shared.repositories.BooksRepository

class UsingBooks(private val repository: BooksRepository) : BooksUseCase {

    override suspend fun getBooksOrderedByRecency()
        = repository.getBooks().mapTo { books -> books.sortedByDescending { it.released } }

}
