package com.xdesign.gameofthrones.core.books

import com.xdesign.gameofthrones.shared.models.Book
import com.xdesign.gameofthrones.shared.results.Result

interface BooksUseCase {
    suspend fun getBooksOrderedByRecency() : Result<Error, List<Book>>
}
