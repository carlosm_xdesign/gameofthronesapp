package com.xdesign.gameofthrones.widgets.booklist

import android.app.Activity
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.xdesign.gameofthrones.application.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class BookListViewShould {

    @get:Rule
    var activityRule = ActivityScenarioRule(TestingBookListViewActivity::class.java)

    private val screen = BooksListViewScreenMapper()

    @Test
    fun hello_world_test() {
        screen {
            screenMessage.isNotDisplayed()
            screenMessage.hasText("")
        }
    }
}

