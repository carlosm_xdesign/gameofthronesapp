package com.xdesign.gameofthrones.widgets.booklist

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.xdesign.gameofthrones.R
import com.xdesign.gameofthrones.databinding.ActivityMainBinding

class TestingBookListViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBinding()
    }

    private fun setupBinding() {
//        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
