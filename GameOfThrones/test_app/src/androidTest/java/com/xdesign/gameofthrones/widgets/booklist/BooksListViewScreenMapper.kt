package com.xdesign.gameofthrones.widgets.booklist

import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KTextView
import com.xdesign.gameofthrones.R

class BooksListViewScreenMapper : Screen<BooksListViewScreenMapper>() {
    val screenMessage = KTextView {
        withId(R.id.screen_message)
    }
}