package com.xdesign.gameofthrones.application.extensions

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.cancel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel() {

    val scope = CoroutineScope(Dispatchers.IO)
    protected var currentJob: Job? = null

    protected fun launchScope(task: suspend () -> Unit) {
        currentJob = scope.launch { task() }
    }

    override fun onCleared() {
        super.onCleared()
        scope.cancel()
    }
}
