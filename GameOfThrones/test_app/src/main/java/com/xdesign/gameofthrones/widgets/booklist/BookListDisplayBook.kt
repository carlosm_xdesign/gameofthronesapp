package com.xdesign.gameofthrones.widgets.booklist

data class BookListDisplayBook(val isbn: String, val name: String, val releasedOn: String)
