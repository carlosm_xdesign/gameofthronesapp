package com.xdesign.gameofthrones.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.xdesign.gameofthrones.application.extensions.BaseViewModel

class HomeViewModel : BaseViewModel() {
    fun onViewReady() {

    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text
}
