package com.xdesign.gameofthrones.application

import android.app.Application
import com.xdesign.gameofthrones.application.modules.KoinBooter
import com.xdesign.gameofthrones.shared.network.Environment

class App : Application() {

    private val environment = Environment(
        apiUrl = "https://anapioficeandfire.com"
    )

    override fun onCreate() {
        super.onCreate()
        KoinBooter.start(this, environment)
    }
}
