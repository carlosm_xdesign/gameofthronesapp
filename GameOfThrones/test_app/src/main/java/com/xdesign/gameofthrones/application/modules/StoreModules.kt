package com.xdesign.gameofthrones.application.modules

import com.xdesign.gameofthrones.core.books.BooksStore
import com.xdesign.gameofthrones.shared.repositories.BooksRepository
import org.koin.dsl.module

val storeModules = module {
    single<BooksRepository> {
        BooksStore(get())
    }
}
