package com.xdesign.gameofthrones.application.modules

import com.xdesign.gameofthrones.core.books.BooksUseCase
import com.xdesign.gameofthrones.core.books.UsingBooks
import com.xdesign.gameofthrones.home.HomeViewModel
import com.xdesign.gameofthrones.widgets.booklist.BookListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val useCaseModules = module {
    factory<BooksUseCase> { UsingBooks(get()) }
}
