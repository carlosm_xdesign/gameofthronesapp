package com.xdesign.gameofthrones.widgets.extentions

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import com.xdesign.gameofthrones.R


/**
 * This view is able to get a ViewModel from the context using a Navhost fragment id.
 * In the layout add:
 *
 * scopeNavHostFragmentId="R.id.whatever_navHost"
 *
 * It achieves this by looking for the current navigation Fragment
 * (or the primary screen Fragment) to use as the context.
 *
 * When you want to have multiple instances with their own view model,
 * then you will need to scope it manually using "scopeTo(yourFragment)".
 * The auto look up for an id in the layout file won't work because
 * the fragment won't be found when doing the lookup.
 */
abstract class ScopedView<VM : ViewModel> : ConstraintLayout {

    constructor(context: Context) : super(context) {
        // This view can only be instantiated with a Context and AttributeSet
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    protected abstract fun initView(context: Context, attrs: AttributeSet?)
    abstract fun createViewModel(fragment: Fragment): VM
    abstract fun onCreatedViewModel(viewModel: VM)

    protected var scopeFragment: Fragment? = null
        private set
    var model: VM? = null

    private fun init(context: Context, attrs: AttributeSet?) {
        initView(context, attrs)
    }

    fun scopeTo(fragment: Fragment) {
        if (scopeFragment == null) {
            scopeFragment = fragment
            initViewModel()
        }
    }

    private fun initViewModel() {
        scopeFragment?.run {
            model = createViewModel(this)
            onCreatedViewModel(model as VM)
        }
    }
}

