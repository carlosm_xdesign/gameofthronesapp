package com.xdesign.gameofthrones.application.epoxy;

import com.airbnb.epoxy.EpoxyDataBindingPattern;
import com.xdesign.gameofthrones.R;


@EpoxyDataBindingPattern(rClass = R.class, layoutPrefix = "view_holder")
interface EpoxyConfig {}
